package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Bee.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class Bee extends Insect {
	private BeeCaste caste;
	
	/**
	 * Instantiates a new bee.
	 * 
	 * @precondition length > 0
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                Color.BLACK && isWinged() == true && getCaste() == caste
	 * 
	 * @param length the length
	 * @param caste the caste
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, true, Color.BLACK);
		this.caste = caste;
	}
	
	/**
	 * Gets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the type
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * Sets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param caste the new case
	 */
	public void setCaste(BeeCaste caste) {
		this.caste = caste;
	}
	
	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Caste: " + this.caste;
		
		return description;
	}
	
	@Override
	public Canvas getDrawing() {
		Canvas canvas = super.getDrawing();
		GraphicsContext gc = canvas.getGraphicsContext2D();
		this.drawStripes(gc);
		return canvas;
	}
	
	@Override
	public Canvas getDrawing(double xCoor, double yCoor) {
		Canvas canvas = super.getDrawing(xCoor, yCoor);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		this.drawStripes(gc);
		return canvas;
	}
	
	private void drawStripes(GraphicsContext gc) {
		gc.setFill(Color.YELLOW);
		gc.fillRect(15, 41, 8, 18);
		gc.fillRect(35, 40, 8, 20);
		gc.fillRect(57, 40, 8, 20);
		gc.fillRect(79, 41, 8, 18);
	}

	@Override
	public String toString() {
		return "Bee type of "  +  super.toString() + " caste=" + this.caste;
	}
}
