package edu.westga.cs1302.bugcensus.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

import edu.westga.cs1302.bugcensus.model.interfaces.Sortable;
import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class BugCensus implements Collection<Bug>, Sortable<Bug> {

	private int year;
	private ArrayList<Bug> bugs;
	private int numberInsects;
	private int numberMyriapodas;

	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition none
	 * @postcondition getBugs.size() == 0 && getNumberInsects() == 0 &&
	 *                getNUmberMyriapods() == 0
	 */
	public BugCensus() {
		Calendar now = Calendar.getInstance();
		this.year = now.get(Calendar.YEAR);
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}

	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year && getBugs.size() == 0 && getNumberInsects()
	 *                == 0 && getNUmberMyriapods() == 0
	 * 
	 * @param year the year
	 */
	public BugCensus(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}

	/**
	 * Gets the year.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year
	 * 
	 * @param year the new year
	 */
	public void setYear(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
	}

	/**
	 * returns the size.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size
	 */
	public int size() {
		return this.bugs.size();
	}

	/**
	 * Adds the bug.
	 *
	 * @precondition bug != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param bug the bug
	 * @return true, if successful
	 */
	@Override
	public boolean add(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		if (this.bugs.add(bug)) {
			if (bug instanceof Insect) {
				this.numberInsects++;
			} else if (bug instanceof Myriapoda) {
				this.numberMyriapodas++;
			}
			return true;
		}
		return false;
	}

	/**
	 * Adds the insect.
	 *
	 * @precondition insect != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param insect the insect
	 * @return true, if successful
	 */
	public boolean add(Insect insect) {
		if (insect == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}
		if (this.bugs.add(insect)) {
			this.numberInsects++;
			return true;
		}
		return false;
	}

	/**
	 * Adds the myriapoda.
	 *
	 * @precondition myriapoda != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param myriapoda the myriapoda
	 * @return true, if successful
	 */
	public boolean add(Myriapoda myriapoda) {
		if (myriapoda == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}
		if (this.bugs.add(myriapoda)) {
			this.numberMyriapodas++;
			return true;
		}
		return false;
	}

	/**
	 * Gets the number insects.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the number insects
	 */
	public int getNumberInsects() {
		return this.numberInsects;
	}

	/**
	 * Gets the number myriapodas.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number myriapodas
	 */
	public int getNumberMyriapodas() {
		return this.numberMyriapodas;
	}

	/**
	 * Gets the summary report.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the summary report
	 */
	public String getSummaryReport() {
		String report = "Bug Census of " + this.year;
		report += System.lineSeparator();
		report += "Total number bugs: " + this.size();
		report += System.lineSeparator();
		report += "Number insects: " + this.getNumberInsects();
		report += System.lineSeparator();
		report += "Total myriapodias: " + this.getNumberMyriapodas();
		return report;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(Object bug) {
		if (bug == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}
		return this.bugs.contains(bug);
	}

	@Override
	public Iterator<Bug> iterator() {
		CensusIterator censusIterator = new CensusIterator();
		return censusIterator;
	}

	@Override
	public Object[] toArray() {
		Bug[] bugs = new Bug[this.bugs.size()];
		for (int i = 0; i < this.bugs.size(); i++) {
			bugs[i] = this.bugs.get(i);
		}
		return bugs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] bugsArray) {
		if (bugsArray == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_CENSUS);
		}
		for (int i = 0; i < this.bugs.size(); i++) {
			bugsArray[i] = (T) this.bugs.get(i);
		}
		return bugsArray;
	}

	@Override
	public boolean remove(Object bug) {
		if (bug == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}
		if (this.bugs.remove(bug)) {
			if (bug instanceof Insect) {
				this.numberInsects--;
			}
			if (bug instanceof Myriapoda) {
				this.numberMyriapodas--;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> bugs) {
		if (bugs == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_CENSUS);
		}
		if (bugs.contains(null)) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}
		for (Object bug : bugs) {
			if (bugs.contains((Bug) bug)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> bugs) {
		if (bugs == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_CENSUS);
		}
		if (bugs.contains(null)) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}
		boolean returnValue = false;
		for (Object bug : bugs) {
			if (!this.bugs.remove(bug)) {
				if (bug instanceof Insect) {
					this.numberInsects--;
				}
				if (bug instanceof Myriapoda) {
					this.numberMyriapodas--;
				}
				returnValue = true;
			}
		}
		return returnValue;
	}

	@Override
	public boolean retainAll(Collection<?> bugs) {
		for (Bug bug : this.bugs) {
			if (bugs.contains(bug)) {
				this.bugs.remove(bug);
			}
		}
		return false;
	}

	@Override
	public void clear() {
		this.bugs.clear();
	}

	@Override
	public boolean addAll(Collection<? extends Bug> bugs) {
		if (bugs == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_CENSUS);
		}
		if (bugs.contains(null)) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}
		int bugsSize = this.bugs.size();
		int addSize = bugs.size();
		for (Object bug : bugs) {
			this.add((Bug) bug);
			if (!this.add((Bug) bug)) {
				if (bug instanceof Insect) {
					this.numberInsects++;
				}
				if (bug instanceof Myriapoda) {
					this.numberMyriapodas++;
				}
			}
		}
		if (this.bugs.size() == bugsSize + addSize) {
			return true;
		}
		return false;
	}

	private final class CensusIterator implements Iterator<Bug> {
		private int currentIndex;

		/**
		 * Instantiates a new CensusIterator
		 */
		private CensusIterator() {
			this.currentIndex = 0;
		}

		@Override
		public boolean hasNext() {
			return this.currentIndex < BugCensus.this.size();
		}

		@Override
		public Bug next() {
			Bug bug = BugCensus.this.bugs.get(this.currentIndex);
			this.currentIndex++;
			return bug;
		}

	}

	@Override
	public void sort() {
		SizeComparator sizeCompare = new SizeComparator();
		this.bugs.sort(sizeCompare);
	}

	/**
	 * class that creates a size comparator allowing bugs to be compared by length
	 * 
	 * @author nicolebranum
	 */
	public class SizeComparator implements Comparator<Bug> {

		@Override
		public int compare(Bug o1, Bug o2) {
			if (o1.getLength() > o2.getLength()) {
				return 1;
			}
			if (o1.getLength() < o2.getLength()) {
				return -1;
			}
			return 0;
		}

	}

	@Override
	public void sort(Comparator<Bug> comp) {
		this.bugs.sort(comp);

	}

}
