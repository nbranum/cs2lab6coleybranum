package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Myriapoda.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class Myriapoda extends Bug {
	private static final double DEFAULT_HEIGHT = 50; 
	private static final double DEFAULT_SEGMENT_WIDTH = 20; 
	private int numberSegments;
	
	/**
	 * Instantiates a new myriapoda.
	 *
	 * @precondition length > 0 && numberLegs >= 0 && numberSegments > 0 &&
	 *               color != null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color && getNumberSegments() == numberSegments
	 * 
	 * @param length the length
	 * @param numberLegs the number legs
	 * @param numberSegments the number segments
	 * @param color the color
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super(length, numberLegs, color);
		if (numberSegments <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_NUMBER_SEGMENTS);
		}
		this.numberSegments = numberSegments;
	}

	/**
	 * Gets the number of segments.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of segments
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}
	
	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Number segements: " + this.numberSegments;
		
		return description;
	}
	
	@Override
	public double getDrawingHeight() {
		double scale = this.getLength() / (this.numberSegments * DEFAULT_SEGMENT_WIDTH);
		double height = scale * DEFAULT_HEIGHT;
		return height;
	}
	
	@Override
	public Canvas getDrawing() {
		double default_width = this.numberSegments * DEFAULT_SEGMENT_WIDTH;
		double width = this.getLength();
		double scale = width / default_width;
		double height = scale * DEFAULT_HEIGHT;
		Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.scale(scale, scale);
		this.draw(gc, default_width);
		return canvas;
	}
	
	@Override
	public Canvas getDrawing(double xCoor, double yCoor) {
		Canvas canvas = this.getDrawing();
		canvas.setLayoutX(xCoor);
		canvas.setLayoutY(yCoor);
		return canvas;
	}
	
	/**
	 * Draws the insect using the default width and height
	 * 
	 * @param gc he GraphicsContext to be drawn on
	 */
	private void draw(GraphicsContext gc, double default_width) {
		gc.setFill(this.getColor());
		gc.setStroke(this.getColor());
		// draw legs
		int halfNumberLegs = this.getNumberLegs() / 2;
		double spacing = default_width / ((double) halfNumberLegs + 1);
		for (int i = 1; i <= halfNumberLegs; i++) {
			gc.strokeLine(i * spacing, 2, i * spacing, DEFAULT_HEIGHT - 2);
		}
		// draw segments
		for (int i = 0; i < this.getNumberSegments(); i++) {
			gc.fillOval(i * DEFAULT_SEGMENT_WIDTH - 2, DEFAULT_HEIGHT / 4, DEFAULT_SEGMENT_WIDTH + 4, DEFAULT_HEIGHT / 2);	
		}
	}
	
	@Override
	public String toString() {
		return "Myriapoda type of " + super.toString() +  " #segments=" + this.numberSegments;
	}
}
