package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Insect.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class Insect extends Bug {
	private static final double DEFAULT_CANVAS_WIDTH = 100;
	private static final double DEFAULT_CANVAS_HEIGHT = 100;
	
	private boolean winged;
	
	/**
	 * Instantiates a new insect.
	 *
	 * @precondition length > 0 && color != null
	 * @postcondition getLength() == length && getNumberLegs() == 6 &&
	 *                getColor() == color && isWinged() == winged
	 * 
	 * @param length the length
	 * @param winged the winged
	  * @param color the color
	 */
	public Insect(double length, boolean winged, Color color) {
		super(length, 6, color);
		this.winged = winged;
	}
	
	/**
	 * Checks if is winged.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if is winged
	 */
	public boolean isWinged() {
		return this.winged;
	}

	/**
	 * Sets the winged.
	 *
	 * @precondition none
	 * @postcondition getWinged() == winged
	 * 
	 * @param winged the new winged
	 */
	public void setWinged(boolean winged) {
		this.winged = winged;
	}
	
	@Override
	public Canvas getDrawing() {
		double width = this.getLength();
		double height = this.getLength();
		Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();	
		double scaleX = width / DEFAULT_CANVAS_WIDTH;
		double scaleY = height / DEFAULT_CANVAS_HEIGHT;
		gc.scale(scaleX, scaleY);
		this.draw(gc);
		return canvas;
	}
	
	@Override
	public Canvas getDrawing(double xCoor, double yCoor) {
		Canvas canvas = this.getDrawing();
		canvas.setLayoutX(xCoor);
		canvas.setLayoutY(yCoor);
		return canvas;
	}
	
	/**
	 * Draws the insect using the default dimension
	 * 
	 * @param gc he GraphicsContext to be drawn on
	 */
	private void draw(GraphicsContext gc) {
		gc.setFill(this.getColor());
		gc.setStroke(this.getColor());
		
		// draw body
		gc.fillOval(0, 40, 100, 20);
		
		// draw legs
		gc.strokeLine(5, 5, 50, 50);
		gc.strokeLine(5, 95, 50, 50);
		gc.strokeLine(55, 0, 50, 50);
		gc.strokeLine(55, 100, 50, 50);
		gc.strokeLine(90, 5, 50, 50);
		gc.strokeLine(90, 95, 50, 50);
		
		// draw wings if insect with wings
		if (this.winged) {
			gc.strokeOval(30, 1, 30, 45);
			gc.strokeOval(30, 54, 30, 45);
		}
	}
	
	@Override
	public String toString() {
		return "Insect type of " + super.toString() + " winged=" + this.winged;
	}
}
