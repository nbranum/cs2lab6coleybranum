package edu.westga.cs1302.bugcensus.model.interfaces;

import java.util.Comparator;

/**
 * The Interface Sortable - can be implemented for collections over type T
 * 
 * @author CS1302
 * @version Spring 2020
 * 
 * @param <T> the type of the items in the collection
 */
public interface Sortable<T> {

	/**
	 * Sorts the collection using the natural order defined on type T
	 */
	void sort();
	
	/**
	 * Sort the collection using the specified comparator defined for type T
	 * 
	 * @param comp the comparator defining the order
	 */
	void sort(Comparator<T> comp);
}
